//Função AJAX para Logar no sistema.
$(document).ready(function(){
        $('#loginGif').hide();
        $('#erroLogin').hide();
            $('#formLogin').submit(function(){  //Ao submeter formulário
                $('#loginGif').show();
                $('#erroLogin').hide();
                var login=$('#usrname').val();
                var senha=$('#psw').val();
                $.ajax({            
                    url:"logim.jsf",                  
                    type:"post",                            
                    data: "login="+login+"&senha="+senha,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==0){
                            $('#loadGif').hide();
                            $("#erroLogin").html("<p class='text-center'>Usuário ou senha incorretos.</p>");
                            $('#erroLogin').show();
                            $('#erroLogin').addClass('animated shake');
                        }if(result==1){
                            $('#loadGif').hide();
                            $("#erroLogin").html("<p class='text-center'>Ops, ocorreu um pequeno erro no Login, tente novamente.</p>");
                            $('#erroLogin').show();
                            $('#erroLogin').addClass('animated shake');
                        }if (result ==2){
                            $('#loadGif').hide();
                            window.location.href = "";
                        }if (result !=0 && result != 1 && result != 2){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Não foi possível realizar o login.</br>Contate o administrador.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }  
                    }
                });
            return false;   //Evita que a página seja atualizada
            });
});

$(document).ready(function(){
  // Código para adicionar suavidade no deslizamento da página através do menu.
  $(".navbar a, footer a[href='#myPage'], .btnEntraEmContato").on('click', function(event) {
    //Certifique-se de que this.hash tem um valor antes de substituir o comportamento padrão
    if (this.hash !== "") {
      // Prevenir o comportamento de clique de âncora padrão
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Usando o método jQuery animate () para adicionar scroll de página suave
      // Um número opcional (900) que significa o número de milissegundos necessários para rolar para a área escolhida.
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Adicionar hash(#) ao URL quando feito scroll (comportamento de clique padrão)
        window.location.hash = hash;
      });
    } // Fim do IF
  });
  

  //Código para quando deslizar a página ele aparecer os elementos.
  $(window).scroll(function() {
    $(".esconderParaDeslizar").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
});

$(document).ready(function(){
    $("#myLogin").click(function(){
        $("#modalLogin").modal();
    });
});

$(document).ready(function(){
  $('.solicitacaoCliente').click(function() {
    $('#modalLogin').modal('hide');

    
      var hash = '#pricing';
    
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
  
        window.location.hash = hash;
      });
  });
});


function mudarIconeExames(div,icone){
    $(div).on("hidden.bs.collapse", function(){
      $(icone).html('<span class="glyphicon glyphicon-menu-down iconeAccordion"></span>');
    });
  
    $(div).on("show.bs.collapse", function(){
      $('.textoAccordion').html('<span class="glyphicon glyphicon-menu-down iconeAccordion"></span>');
      $(icone).html('<span class="glyphicon glyphicon-menu-up iconeAccordion"></span>');
      $(icone).html('<span class="glyphicon glyphicon-menu-up iconeAccordion"></span>');
    });
}

function mudarIconePlanos(div,icone){
    $(div).on("hidden.bs.collapse", function(){
      $(icone).html('<span class="glyphicon glyphicon-menu-down iconeAccordion"></span>');
    });
  
    $(div).on("show.bs.collapse", function(){
      $('.setaPlanos').html('<span class="glyphicon glyphicon-menu-down iconeAccordion"></span>');
      $(icone).html('<span class="glyphicon glyphicon-menu-up iconeAccordion"></span>');
      $(icone).html('<span class="glyphicon glyphicon-menu-up iconeAccordion"></span>');
    });
}


//Função AJAX para contato
$(document).ready(function(){
        $('#loadGif').hide();
        $('#sucessoEnvio').hide();
        $('#erroEnvio').hide();
        $('.imputForm').hide();
            $('#formularioContato').submit(function(){  //Ao submeter formulário
                $('#loadGif').show();
                $('#sucessoEnvio').hide();
                $('#erroEnvio').hide();
                var nome=$('#nome').val();
                var email=$('#email').val();
                var comentario=$('#comentario').val();
                $.ajax({            //Função AJAX
                    url:"email.php",                    //Arquivo php
                    type:"post",                            //Método de envio
                    data: "nome="+nome+"&email="+email+"&comentario="+comentario,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==0){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Erro de inserção na base de dados.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }if(result==1){
                            $('#loadGif').hide();
                            $('#fieldsetForm').attr("disabled", "disabled");
                            $('.imputForm').show();
                            $(".form-group").addClass("has-success has-feedback");
                            //$(".imputForm").append("<span class='glyphicon glyphicon-ok form-control-feedback'></span>");
                            
                            $('#sucessoEnvio').show();
                            $('#sucessoEnvio').addClass('animated shake');                   
                        }if (result ==2){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Ops, Um dos contatos não foi enviado.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }if (result !=0 && result != 1 && result != 2){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Ops, ocorreu um pequeno erro no envio do e-mail, mas não tem problema.</br>Você ainda pode ligar para nosso número: (81) 3471-6198.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }  
                    }
                });
            return false;   //Evita que a página seja atualizada
            });
});

/*

$(document).ready(function(){
    $("#collapse1").on("hidden.bs.collapse", function(){
      $(".setaAccordion1").html('Anamnese <span class="glyphicon glyphicon-plus iconeAccordion"></span>');
    });
  
    $("#collapse1").on("show.bs.collapse", function(){
      $(".setaAccordion1").html('Anamnese <span class="glyphicon glyphicon-minus iconeAccordion"></span>');
      $('#collapse2').collapse('hide');
      $('#collapse3').collapse('hide');
      $('#collapse4').collapse('hide');
      $('#collapse5').collapse('hide');
      $('#collapse6').collapse('hide');
    });
  });

$(document).ready(function(){
    $("#collapse2").on("hidden.bs.collapse", function(){
      $(".setaAccordion2").html('Par-Q <span class="glyphicon glyphicon-plus iconeAccordion"></span>');
    });
  
    $("#collapse2").on("show.bs.collapse", function(){
      $(".setaAccordion2").html('Par-Q <span class="glyphicon glyphicon-minus iconeAccordion"></span>');
      $('#collapse1').collapse('hide');
      $('#collapse3').collapse('hide');
      $('#collapse4').collapse('hide');
      $('#collapse5').collapse('hide');
      $('#collapse6').collapse('hide');
    });
  });

$(document).ready(function(){
    $("#collapse3").on("hidden.bs.collapse", function(){
      $(".setaAccordion3").html('Risco Coronariano <span class="glyphicon glyphicon-plus iconeAccordion"></span>');
    });
  
    $("#collapse3").on("show.bs.collapse", function(){
      $(".setaAccordion3").html('Risco Coronariano <span class="glyphicon glyphicon-minus iconeAccordion"></span>');
      $('#collapse1').collapse('hide');
      $('#collapse2').collapse('hide');
      $('#collapse4').collapse('hide');
      $('#collapse5').collapse('hide');
      $('#collapse6').collapse('hide');
    });
  });



$(document).ready(function(){
    $("#collapse4").on("hidden.bs.collapse", function(){
      $(".setaAccordion4").html('Outro Exame <span class="glyphicon glyphicon-plus iconeAccordion"></span>');
    });
  
    $("#collapse4").on("show.bs.collapse", function(){
      $(".setaAccordion4").html('Outro Exame <span class="glyphicon glyphicon-minus iconeAccordion"></span>');
      $('#collapse1').collapse('hide');
      $('#collapse2').collapse('hide');
      $('#collapse3').collapse('hide');
      $('#collapse5').collapse('hide');
      $('#collapse6').collapse('hide');
    });
  });

$(document).ready(function(){
    $("#collapse5").on("hidden.bs.collapse", function(){
      $(".setaAccordion5").html('Outro Exame <span class="glyphicon glyphicon-plus iconeAccordion"></span>');
    });
  
    $("#collapse5").on("show.bs.collapse", function(){
      $(".setaAccordion5").html('Outro Exame <span class="glyphicon glyphicon-minus iconeAccordion"></span>');
      $('#collapse1').collapse('hide');
      $('#collapse2').collapse('hide');
      $('#collapse3').collapse('hide');
      $('#collapse4').collapse('hide');
      $('#collapse6').collapse('hide');
    });
  });

$(document).ready(function(){
    $("#collapse6").on("hidden.bs.collapse", function(){
      $(".setaAccordion6").html('Outro Exame <span class="glyphicon glyphicon-plus iconeAccordion"></span>');
    });
  
    $("#collapse6").on("show.bs.collapse", function(){
      $(".setaAccordion6").html('Outro Exame <span class="glyphicon glyphicon-minus iconeAccordion"></span>');
      $('#collapse1').collapse('hide');
      $('#collapse2').collapse('hide');
      $('#collapse3').collapse('hide');
      $('#collapse4').collapse('hide');
      $('#collapse5').collapse('hide');
    });
  });
  */